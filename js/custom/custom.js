// JavaScript Document


$(document).ready(function() {

    'use strict';

    

    /************************************************************************************ CAROUSEL SLIDER STARTS */

    var owl = $('.services-carousel');
    owl.owlCarousel({

        autoplay: false,
        autoplayHoverPause: true,
        nav: false,
        dots: true,
        mouseDrag: true,
        smartSpeed: 500,
        margin: 0,
        loop: true,
        singleItem: true,
        navText: [
            "<i class='fa fa-angle-right'></i>", "<i class='fa fa-angle-left'></i>"
        ],
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            1000: {
                items: 3
            }
        }
    });
	
	var owl = $('.services-carousel-02');
    owl.owlCarousel({

        autoplay: false,
        autoplayHoverPause: true,
        nav: false,
        dots: true,
        mouseDrag: true,
        smartSpeed: 500,
        margin: 0,
        loop: true,
        singleItem: true,
        navText: [
            "<i class='fa fa-angle-right'></i>", "<i class='fa fa-angle-left'></i>"
        ],
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            1000: {
                items: 4
            }
        }
    });
	
	var owl = $('.team-carousel-02');
    owl.owlCarousel({

        autoplay: false,
        autoplayHoverPause: true,
        nav: true,
        dots: false,
        mouseDrag: true,
        smartSpeed: 500,
        margin: 0,
        loop: true,
        singleItem: true,
        navText: [
            "<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"
        ],
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 5
            }
        }
    });
	
	var owl = $('.clients-carousel');
    owl.owlCarousel({

        autoplay: false,
        autoplayHoverPause: true,
        nav: false,
        dots: true,
        mouseDrag: true,
        smartSpeed: 500,
        margin: 0,
        loop: true,
        singleItem: true,
        navText: [
            "<i class='fa fa-angle-right'></i>", "<i class='fa fa-angle-left'></i>"
        ],
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 5
            }
        }
    });
	
	var owl = $('.simple-pics-carousel');
    owl.owlCarousel({

        autoplay: false,
        autoplayHoverPause: true,
        nav: true,
        dots: false,
        mouseDrag: true,
        smartSpeed: 500,
        margin: 0,
        loop: true,
        singleItem: true,
        navText: [
            "<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"
        ],
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });
	
	var owl = $('.product-carousel');
    owl.owlCarousel({

        autoplay: false,
        autoplayHoverPause: true,
        nav: false,
        dots: true,
        mouseDrag: true,
        smartSpeed: 500,
        margin: 0,
        loop: true,
        singleItem: true,
        navText: [
            "<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"
        ],
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });
	

    /************************************************************************************ SLIDER CAROUSEL ENDS */

	 /************************************************************************************ STICKEY NAV STARTS */
	 
	$(window).scroll(function() {
		if ($(this).scrollTop() > 1){  
			$('nav').addClass("sticky");
		}
		else{
			$('nav').removeClass("sticky");
		}
	});
		
	
	/************************************************************************************ STICKEY NAV ENDS */
	
	/************************************************************************************ BX SLIDER STARTS */
	
	$('.bxslider').bxSlider({
		pagerCustom: '.bx-pager'
	});
	
	/************************************************************************************ BX SLIDER ENDS */
	
	/************************************************************************************ TEAM CAROUSEL STARTS */
	
	if ($('.team-carousel').length > 0) {
			
		$('.team-carousel').slick({
			infinite: true,
			slidesToShow: 1,
			centerMode: true,
			variableWidth: true,
			prevArrow: $('#team-slideshow-prev'),
			nextArrow: $('#team-slideshow-next')
		});
	}
	
	/************************************************************************************ TEAM CAROUSEL ENDS */
	
	
	/************************************************************************************ PARALLAX STARTS */
	
	$('.parallax-1, .parallax-2, .parallax-3, .parallax-4, .parallax-5').parallax("50%", 0.1);

    /************************************************************************************ PARALLAX ENDS */
    
	/************************************************************************************ FITVID STARTS */

    $(".fitvid").fitVids();

    /************************************************************************************ FITVID ENDS */

    /************************************************************************************ TO TOP STARTS */

    $(window).scroll(function() {
        if ($(this).scrollTop() > 100) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    });

    $('.scrollup').on("click", function() {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });

    /************************************************************************************ TO TOP ENDS */

	    /************************************************************************************ COUNTER UP STARTS */

    $('.count').counterUp({
        delay: 10,
        time: 1000
    });

    /************************************************************************************ COUNTER UP ENDS */


    /************************************************************************************ MAGNIFIC POPUP STARTS */

    $('.image-popup-vertical-fit').magnificPopup({
		type: 'image',
		closeOnContentClick: true,
		mainClass: 'mfp-img-mobile',
		image: {
			verticalFit: true
		}
		
	});

    $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
        disableOn: 700,
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,

        fixedContentPos: false
    });

    /************************************************************************************ MAGNIFIC POPUP ENDS */

	$('#preloader').fadeOut('slow');


}); //$(document).ready(function () {


